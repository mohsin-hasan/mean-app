import {
	Component,
	Output,
	EventEmitter,
	ViewChild,
	ElementRef,
	OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { List } from '../models/list';
import { ListService } from '../list.service';



@Component({
	selector: 'app-add-list',
	templateUrl: './add-list.component.html',
	styleUrls: ['./add-list.component.css']
})

export class AddListComponent implements OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy {
	@Output() addList: EventEmitter<List> = new EventEmitter<List>();
	@ViewChild('addNewItemForm') addNewForm: NgForm;

	private newList: List;

	constructor(private listService: ListService, private elementRef: ElementRef) { }

	ngOnInit() {
		// console.log("Child-2's ngOnInit");
		this.newList = {
			title: '',
			category: '',
			description: '',
			_id: ''
		};
	}

	public onSubmit() {
		this.listService.addList(this.newList)
			.subscribe(res => {
				if (res.success) {
					const newListItem = Object.assign({}, res.list);
					this.addList.emit(newListItem);
				}
			});
	}

	resetForm() {
		this.addNewForm.form.reset();
	}

	/** Methods to just to check Angular LifeCycle Hooks implementation */
	ngOnChanges() {
		// console.log("Child-2's ngOnChanges");
	}
	ngDoCheck() {
		// console.log("Child-2's ngDoCheck");
	}
	ngAfterContentInit() {
		// console.log("Child-2's ngAfterContentInit");
	}
	ngAfterContentChecked() {
		// console.log("Child-2's ngAfterContentChecked");
	}
	ngAfterViewInit() {
		// console.log("Child-2's ngAfterViewInit");
	}
	ngAfterViewChecked() {
		// console.log("Child-2's ngAfterViewChecked");
	}
	ngOnDestroy() {
		// console.log("Child-2's ngOnDestroy");
	}
}
