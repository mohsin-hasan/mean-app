const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';
const bucketList = require('../model/list-model');

/* GET api listing. */
// router.get('/', (req, res) =>{
//     res.send('api works');
// });

router.get('/', (req, res) => {
    bucketList.getAllLists((err, lists) => {
        if (err) {
            res.json({
                success: false,
                message: `Failed to load all lists. Error: ${err}`
            });
        }
        else {
            res.write(JSON.stringify({ success: true, lists: lists }, null, 2));
            res.end();
        }
    });
});

router.get('/posts', (req, res) => {
    axios.get(`${API}/posts`).then(posts => {
        res.status(200).json(posts.data);
    })
        .catch(error => {
            res.status(500).send(error);
        });
});

router.post('/', (req, res, next) => {
    let newList = new bucketList({
        title: req.body.title,
        description: req.body.description,
        category: req.body.category
    });

    bucketList.addList(newList, (err, list) => {
        if (err) {
            res.json({ success: false, message: `Failed to create a new list. Error: ${err}` });
        }
        else
            res.json({ success: true, message: "Added successfully." });
    });
});

router.delete('/:id', (req, res, next) => {
    // res.delete('Delete');
    //access the parameter which is the id of the item to be deleted
    let id = req.params.id;
    //Call the model method deleteListById
    bucketList.deleteListById(id, (err, list) => {
        if (err) {
            res.json({ success: false, message: `Failed to delete the list. Error: ${err}` });
        }
        else if (list) {
            res.json({ success: true, message: "Deleted successfully" });
        }
        else
            res.json({ success: false });
    })
});

module.exports = router;