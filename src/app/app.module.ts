import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
import { AddListComponent } from './add-list/add-list.component';
import { ViewListComponent } from './view-list/view-list.component';
import { ListService } from './list.service';
import { EditListComponent } from './edit-list/edit-list.component';

import { ModalModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule } from 'angular2-toaster';

// Define the routes
// const ROUTES = [
// 	{
// 		path: '',
// 		redirectTo: 'posts',
// 		pathMatch: 'full'
// 	},
// 	{
// 		path: 'posts',
// 		component: PostsComponent
// 	}
// ];

@NgModule({
	declarations: [
		AppComponent,
		PostsComponent,
		AddListComponent,
		ViewListComponent,
		EditListComponent
	],
	imports: [
		BrowserModule,
		HttpModule,		// RouterModule.forRoot(ROUTES)
		FormsModule,
		ModalModule.forRoot(),
		BrowserAnimationsModule,
		ToasterModule
	],
	providers: [PostsService, ListService],
	bootstrap: [AppComponent]
})
export class AppModule { }
