import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ListService } from '../list.service';
import { List } from '../models/list';

@Component({
	selector: 'app-edit-list',
	templateUrl: './edit-list.component.html',
	styleUrls: ['./edit-list.component.css']
})
export class EditListComponent implements OnInit {

	@Input() listItem;
	@Output() editListItem: EventEmitter<List> = new EventEmitter<List>();
	@ViewChild('editItemForm') editForm: NgForm;

	constructor(private listService: ListService) { }

	ngOnInit() {
	}

	onUpdate() {
		this.listService.updateListItem(this.listItem).subscribe(res => {
			console.log('Res after update :' , res);
			this.editListItem.emit(res);
		});
	}

	resetForm() {
		this.editForm.form.reset();
	}
}
