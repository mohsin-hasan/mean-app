import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import { List } from './models/list';

@Injectable()
export class ListService {

	constructor(private http: Http) { }

	private serverApi = 'http://localhost:3000';

	public getAllLists(): Observable<List[]> {
		let url = `${this.serverApi}/bucketlist/`;
		// const url = '/bucketlist/';
		return this.http.get(url)
			.map(res => res.json())
			.map(res => <List[]>res.lists);
	}

	public deleteList(listId: string) {
		let url = `${this.serverApi}/bucketlist/${listId}`;
		// const url = '/bucketlist/' + listId;

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');

		return this.http.delete(url, { headers })
			.map(res => res.json());
	}

	public addList(list: List) {
		let url = `${this.serverApi}/bucketlist/`;
		// const url = '/bucketlist/';
		let headers = new Headers;
		let body = JSON.stringify({ title: list.title, description: list.description, category: list.category });
		console.log(body);
		headers.append('Content-Type', 'application/json');

		return this.http.post(url, body, { headers: headers })
			.map(res => res.json());
	}

	public updateListItem(listItem: List) {
		let url = `${this.serverApi}/bucketlist/update`;
		// const url = '/bucketlist/update';

		let headers = new Headers;
		let body = JSON.stringify({ id: listItem._id, title: listItem.title, description: listItem.description, category: listItem.category });
		console.log(body);
		headers.append('Content-Type', 'application/json');

		return this.http.post(url, body, { headers: headers })
			.map(res => res.json());
	}

}
